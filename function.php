<?php

class Produk {
public $Makanan;
public $Minuman;
public $jmlMakanan;
public $jmlMinuman;
public $hargaMakanan;
public $hargaMinuman;
public $totalSeluruh;
public $totalHargaMakanan;
public $totalHargaMinuman;
 function __construct() {
  $this->hargaMinuman = 20000;
  $this->hargaMakanan = 50000;
 }
}
// Copyright &copy; 2019. Erji Ridho Lubis(www.portalcoding.com)
class Jumlah extends Produk {
 public function getJumlah($jmlMakanan,$jmlMinuman){
  $this->jmlMakanan = $jmlMakanan;
  $this->jmlMinuman = $jmlMinuman;
 }

 public function setHarga() {
  $this->totalHargaMakanan = $this->hargaMakanan * $this->jmlMakanan;
  $this->totalHargaMinuman = $this->hargaMinuman * $this->jmlMinuman;
  $this->totalSeluruh = $this->totalHargaMakanan + $this->totalHargaMinuman;

 }

 public function Finaltotal() {
  echo "=================&& Struk Total Pembelian $$=================";
  echo "<br>";
  echo "Harga 1 Makanan = Rp. ".$this->hargaMakanan.",-";
  echo "<br>";
  echo "Jumlah Makanan yang di beli = ".$this->jmlMakanan." Set";
  echo "<br>";
  echo "Total Harga Makanan = Rp. ".$this->totalHargaMakanan.",-";
  echo "<br>";
  echo "<br>";
  echo "Harga 1 Minuman = Rp. ".$this->hargaMinuman.",-";
  echo "<br>";
  echo "Jumlah Minuman yang di beli = ".$this->jmlMinuman." Set";
  echo "<br>";
  echo "Total Harga Minuman = Rp. ".$this->totalHargaMinuman.",-";
  echo "<br><br>";
  echo "Total Keseluruhan = Rp. ".$this->totalSeluruh.",-";
  echo "<br>";
  echo "========================================================";
  echo "<br>";
  echo "<br>";
 }
}

 ?>