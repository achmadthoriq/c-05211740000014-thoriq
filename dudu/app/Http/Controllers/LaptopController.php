<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laptop;

class LaptopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laptop = Laptop::all()->toArray();  
		return view('index', compact('laptop')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coba'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  
	{ $laptop = $this->validate(request(), 
	[  
		'merklaptop' => 'required',  
		'hargalaptop' => 'required|numeric' , 
		'jumlah' => 'required',
		'keterangan' => 'required'
	]);  
 
	Laptop::create($laptop);  
 
	 return back()->with('success', 'Product has been added');;  
	} 
 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $laptop = Laptop::find($id);  
		return view('detail', compact('laptop','id')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laptop = Laptop::find($id);  
		return view('edit', compact('laptop','id')); 
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $laptop = Laptop::find($id);  $this->validate(request(), 
	[  'merklaptop' => 'required',  'hargalaptop' => 'required|numeric', 'jumlah' => 'required|numeric' ,  'keterangan' => 'required'   ]);  
       $laptop->merklaptop = $request->get('merklaptop');  $laptop->hargalaptop = $request ->get('hargalaptop');
		$laptop->jumlah = $request->get('jumlah');
		$laptop->keterangan = $request->get('keterangan');
		$laptop->save();  return redirect('laptop')->with('success','Product has been updated'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $laptop = Laptop::find($id);  $laptop->delete();  
		return redirect('laptop')->with('success','Product has been  deleted'); 
    }
}
