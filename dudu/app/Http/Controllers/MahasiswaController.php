<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $mahasiswa = Mahasiswa::all()->toArray();  
		return view('indexmahasiswa', compact('mahasiswa')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        return view('buat');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $mahasiswa = Mahasiswa::find($id);  
		return view('detailmahasiswa', compact('mahasiswa','nrp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nrp)
    {
         $mahasiswa = Mahasiswa::find($nrp);  
		return view('editmahasiswa', compact('mahasiswa','nrp')); 
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatemahasiswa(Request $request, $nrp)
    {
        $mahasiswa = Mahasiswa::find($nrp);  $this->validate(request(), 
	[  'nrp' => 'required',  'nama' => 'required', 'jurusan' => 'required' ,  'ipk' => 'required'   ]);  
        $mahasiswa->NRP = $request->get('nrp');  
		$mahasiswa->Nama = $request ->get('nama');
		$mahasiswa->Jurusan = $request->get('jurusan');
		$mahasiswa->IPK = $request->get('ipk');
		$mahasiswa->save();  return redirect('mahasiswa')->with('success','Product has been updated'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
