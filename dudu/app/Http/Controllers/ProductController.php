<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product; 

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $product = Product::all()->toArray();  
		return view('indexproduct', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  { 
		$product = $this->validate(request(), [  
			'name' => 'required',  'price' => 'required|numeric'  ]);  
 
Product::create($product);  
 
return back()->with('success', 'Product has been added');;  
} 
 
    public function show($id)
    {
        $product = Product::find($id);  
		return view('detailproduct', compact('product','id')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $product = Product::find($id);  
		return view('editproduct', compact('product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);  $this->validate(request(), 
	[  'name' => 'required',  'price' => 'required|numeric' ]); 
		
        $product->name = $request->get('name');  
		$product->price = $request ->get('price');
		$product->save();  return redirect('product')->with('success','Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);  $product->delete();  
		return redirect('product')->with('success','Product has been  deleted');
    }
}
