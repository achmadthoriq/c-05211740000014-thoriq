@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
				<div align="center" class="row">
					<div class="col-md-12">
				 <a href="{{action('ProductController@index')}}"  class="btn btn-primary">Tabel Index Produk</a> </div> </div> <br>
					
						<div class="row">
					
						<div align="center" class="col-md-12">
				 <a href="{{action('LaptopController@index')}}"  class="btn btn-primary">Tabel Index Laptop</a>
						</div> </div>
				
            </div>
        </div>
    </div>
</div>
@endsection
