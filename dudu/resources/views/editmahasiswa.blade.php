<!DOCTYPE html>  
<html>  
	<head>  
	<meta charset="utf-8">  
<title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  
<link rel="stylesheet" href="{{asset('css/app.css')}}">  </head> 
<body>  
<div class="container">  <h2>Perubahan Produk</h2><br /> 
@if ($errors->any())  <div class="alert alert-danger">  <ul>  @foreach ($errors->all() as $error)  <li>{{ $error }}</li>  @endforeach  </ul>  </div><br />  @endif  
<form method="post" action="{{action('MahasiswaController@update', $nrp)}}">  
{{csrf_field()}}
	
<input name="_method" type="hidden" value="PATCH">  
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="merklaptop">NRP:</label>  
		<input type="text" class="form-control" name="nrp" value="{{$mahasiswa->nrp}}">  </div>  
	</div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="hargalaptop">Nama:</label>  
		<input type="text" class="form-control" name="nama" value="{{$mahasiswa->nama}}">  </div>  
	</div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="jumlah">Jurusan:</label>  
		<input type="text" class="form-control" name="jurusan" value="{{$mahasiswa->jurusan}}">  </div>  
	</div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="ipk">IPK:</label>  
		<input type="text" class="form-control" name="ipk" value="{{$mahasiswa->ipk}}">  </div>  
	</div> 
	
	
	 <div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  <button  type="submit"  class="btn  btn-success"  style="margin-  left:38px">Update Produk</button>  </div>  </div>

</form>
</div>
</body>  
</html>  











