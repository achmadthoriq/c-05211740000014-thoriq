<!DOCTYPE html>  
<html>  
	<head>  
	<meta charset="utf-8">  
<title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  
<link rel="stylesheet" href="{{asset('css/app.css')}}">  </head> 
<body>  
<div class="container">  <h2>Detail Product</h2><br /> 
@if ($errors->any())  <div class="alert alert-danger">  <ul>  @foreach ($errors->all() as $error)  <li>{{ $error }}</li>  @endforeach  </ul>  </div><br />  @endif  
<form method="post" action="{{action('ProductController@show', $id)}}">  
{{csrf_field()}}
	
    <input name="_method" type="hidden" value="PATCH">  
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="name" > Nama : {{$product->name}}</label>  
    </div> </div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="price" >Harga: {{ 'Rp.'.number_format($product->price,0,'','.')}} </label> </div>  
	</div> 
	
	<a href="{{action('ProductController@index', $product['id'])}}"  class="btn btn-warning">Kembali</a>

</form>
</div>
</body>  
</html>  











