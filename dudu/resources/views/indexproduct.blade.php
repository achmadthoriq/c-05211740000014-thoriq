<!DOCTYPE html>  
<html>  <head>  
<meta charset="utf-8">  
	<title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  
	<link rel="stylesheet" href="{{asset('css/app.css')}}">  
</head>  
<body>  
<div class="container">  <br />  @if (\Session::has('success'))  
	<div class="alert alert-success">  <p>{{ \Session::get('success') }}</p>  </div><br />  @endif  <table class="table table-striped">  <thead>  
	<tr>  
	<th>ID</th>  
	<th>name</th> 
	<th>price</th>   
	<th colspan="3">Action</th>  </tr>  
	</thead>  
		<tbody>  @foreach($product as $product)  
			<tr>  
				<td>{{$product['id']}}</td> 
				<td>{{$product['name']}}</td>
				<td>{{$product['price']}}</td> 
				
				<td><a href="{{action('ProductController@show', $product['id'])}}"  class="btn btn-primary">Detail</a></td> 
			
				
				<td><a href="{{action('ProductController@edit', $product['id'])}}"  class="btn btn-warning">Ubah</a></td> 
				
				<td>  
				<form  action="{{action('ProductController@destroy', $product['id'])}}" method="post">  {{csrf_field()}} 
						
				<input name="_method" type="hidden" value="DELETE">  
				<button class="btn btn-danger" type="submit">Hapus</button>  </form>  </td>  </tr>  @endforeach  			
			
<!--
			<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Dashboard Index</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="{{action('ProductController@create', $product['id'])}}"  class="btn btn-success">Buat</a></li>
      <li><a href="{{action('HomeController@index')}}"  class="btn btn-success">Home</a></li>
    </ul>
  </div>
</nav>
-->
			
			    <a href="{{action('ProductController@create', $product['id'])}}"  class="btn btn-success">Buat</a> 
			    &nbsp;
			
			   <a href="{{action('HomeController@index')}}"  class="btn btn-success">Home</a>
			
			
</tbody>  
</table>  
</div>  
</body>  
</html>  







