<!DOCTYPE html>  
<html>  <head>  
<meta charset="utf-8">  
	<title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  
	<link rel="stylesheet" href="{{asset('css/app.css')}}">  
</head>  
<body>  
<div class="container">  <br />  @if (\Session::has('success'))  
	<div class="alert alert-success">  <p>{{ \Session::get('success') }}</p>  </div><br />  @endif  <table class="table table-striped">  <thead>  
	<tr>  
	<th>ID</th>  
	<th>merklaptop</th> 
	<th>hargalaptop</th>  
	<th>jumlah</th>  
	<th>keterangan</th>   
	<th colspan="3">Action</th>  </tr>  
	</thead>  
		<tbody>  @foreach($laptop as $laptop)  
			<tr>  
				<td>{{$laptop['id']}}</td> 
				<td>{{$laptop['merklaptop']}}</td>
				<td>{{$laptop['hargalaptop']}}</td> 
				<td>{{$laptop['jumlah']}}</td> 
				<td>{{$laptop['keterangan']}}</td>
				
				<td><a href="{{action('LaptopController@show', $laptop['id'])}}"  class="btn btn-primary">Detail</a></td> 
				
				<td><a href="{{action('LaptopController@edit', $laptop['id'])}}"  class="btn btn-warning">Ubah</a></td> 
				
				<td>  
				<form  action="{{action('LaptopController@destroy',  $laptop['id'])}}" method="post">  {{csrf_field()}} 
						
				<input name="_method" type="hidden" value="DELETE">  
				<button class="btn btn-danger" type="submit">Hapus</button>  </form>  </td>  </tr>  @endforeach  
			
			    <a href="{{action('LaptopController@create', $laptop['id'])}}"  class="btn btn-success">Buat</a>
			    &nbsp;
			
			    <a href="{{action('HomeController@index')}}"  class="btn btn-success">Home</a>
			
			
</tbody>  
</table>  
</div>  
</body>  
</html>  







