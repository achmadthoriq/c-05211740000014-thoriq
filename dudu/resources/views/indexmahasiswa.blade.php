<!DOCTYPE html>  
<html>  <head>  
<meta charset="utf-8">  
	<title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  
	<link rel="stylesheet" href="{{asset('css/app.css')}}">  
</head>  
<body>  
<div class="container">  <br />  @if (\Session::has('success'))  
	<div class="alert alert-success">  <p>{{ \Session::get('success') }}</p>  </div><br />  @endif  <table class="table table-striped">  <thead>  
	<tr>  
	<th>nrp</th>  
	<th>nama</th> 
	<th>jurusan</th>  
	<th>ipk</th>    
	<th colspan="2">Action</th>  </tr>  
	</thead>  
		<tbody>  @foreach($mahasiswa as $mahasiswa)  
			<tr>  
				<td>{{$mahasiswa['nrp']}}</td> 
				<td>{{$mahasiswa['nama']}}</td>
				<td>{{$mahasiswa['jurusan']}}</td> 
				<td>{{$mahasiswa['ipk']}}</td> 
			</tr>
			
				<td><a href="{{action('MahasiswaController@show', $mahasiswa['nrp'])}}"  class="btn btn-primary">View</a></td> 
				
				<td><a href="{{action('MahasiswaController@edit', $mahasiswa['nrp'])}}"  class="btn btn-warning">Edit</a></td> 
			 {{csrf_field()}} 
			    @endforeach 
				
				
			
</tbody>  
</table>  
</div>  
</body>  
</html>  

