<!-- create.blade.php -->  
 
<!DOCTYPE html>  <html>  
<head>  
 
<meta charset="utf-8">  <title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  <link rel="stylesheet" href="{{asset('css/app.css')}}">  </head>  
<body> 
<div class="container">  <h2>Jual Laptop</h2><br/>  
 
@if ($errors->any())  <div class="alert alert-danger">  <ul>  @foreach ($errors->all() as $error)  <li>{{ $error }}</li>  @endforeach  </ul>  </div><br />  @endif  
@if (\Session::has('success'))  <div class="alert alert-success">  <p>{{ \Session::get('success') }}</p>  </div><br />  @endif  
 
<form method="post" action="{{url('laptop')}}">  
{{csrf_field()}}  
	<div class="row">  <div class="col-md-4"></div>  <div class="form-group col-md-4">  <label for="merklaptop">merklaptop:</label>  
		<input type="text" class="form-control" name="merklaptop">  </div> 
	</div>  
	<div class="row">  <div class="col-md-4"></div>  <div class="form-group col-md-4">  <label for="hargalaptop">hargalaptop:</label>  
		<input type="text" class="form-control" name="hargalaptop">  </div>  
	</div>
	
	<div class="row">  <div class="col-md-4"></div>  <div class="form-group col-md-4">  <label for="jumlah">jumlah:</label>  
		<input type="text" class="form-control" name="jumlah">  </div>  
	</div> 
	
	<div class="row">  <div class="col-md-4"></div>  <div class="form-group col-md-4">  <label for="keterangan">keterangan:</label>  
		<input type="text" class="form-control" name="keterangan">  </div>  
	</div> 
	 
	
	<div class="row">  <div class="col-md-4"></div>  <div class="form-group col-md-4">  <button type="submit" class="btn btn-success" style="margin-  left:38px">Tambahkan Produk</button>
		
	<a href="{{action('LaptopController@index')}}"  class="btn btn-warning">Kembali</a></div>  </div>  
 
</form>  
	</div>  
	</body> 
</html>  
 
 
 
 
