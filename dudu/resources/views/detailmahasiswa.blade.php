<!DOCTYPE html>  
<html>  
	<head>  
	<meta charset="utf-8">  
<title>..:: RAI 2017 - Penerapan CRUD pada Laravel 5.5 ::..</title>  
<link rel="stylesheet" href="{{asset('css/app.css')}}">  </head> 
<body>  
<div class="container">  <h2>Detail Laptop</h2><br /> 
@if ($errors->any())  <div class="alert alert-danger">  <ul>  @foreach ($errors->all() as $error)  <li>{{ $error }}</li>  @endforeach  </ul>  </div><br />  @endif  
<form method="post" action="{{action('MahasiswaController@show', $nrp)}}">  
{{csrf_field()}}
	
    <input name="_method" type="hidden" value="PATCH">  
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="nrp" > NRP : {{$mahasiswa->nrp}}</label>  
    </div> </div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="nama" >Nama:  {{$mahasiswa->nama}} </label> </div>  
	</div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="jurusan" >Jurusan:  {{$mahasiswa->jurusan}} </label> </div>  
	</div> 
	
	<div class="row">  <div class="col-md-4"></div>  
	<div class="form-group col-md-4">  
	<label for="ipk" >IPK:  {{$mahasiswa->ipk}} </label> </div>  
	</div> 
	
	 
	</div> 
	<a href="{{action('LaptopController@index', $laptop['id'])}}"  class="btn btn-warning">Kembali</a>

</form>
</div>
</body>  
</html>  











